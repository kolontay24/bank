import math
import datetime
import unittest
import sys

'''Расчет простых процентов.
S = (P x I x t / K) / 100.
I – годовая процентная ставка
t – количество дней начисления процентов по привлеченному вкладу
K – количество дней в календарном году (365 или 366)
P – первоначальная сумма привлеченных в депозит денежных средств
S – сумма начисленных процентов.
'''
my_date = datetime.date.today()
year = my_date.year

if year % 4 != 0 or (year % 100 == 0 and year % 400 != 0):
    k = 365
else:
    k = 366


class Bank():
    def bank(t, p):
        if (p < 0):
            sys.exit("first_sum is incorrect value: " + str(p))

        if (t < 0):
            sys.exit("Count_date is incorrect value: " + str(t))

        if p <= 100:
            i = 3
        elif p > 1000:
            i = 7
        else:
            i = 5

        s = (p * i * t / k) / 100
        S = round(s, 2)
        return S

'''Check S
test= Bank.bank(31, 1000)
print(test)
'''



from bank import Bank
import pytest

test_value = [0, 1, 75.674, 100, 100.01, 1000, 1000.1, "55", 25, 5 * 55, 100000000000000000]


def test_s():
    for test in test_value:
        assert Bank.bank(31, test) >= 0


def test_match():
    assert Bank.bank(31, 1) == 0.0
    assert Bank.bank(31, 10) == 0.03
    assert Bank.bank(31, 999) == 4.24
    assert Bank.bank(31, 1000) == 4.25
